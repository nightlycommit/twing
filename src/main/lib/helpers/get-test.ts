import type {TwingTest} from "../test";
import {TwingSynchronousTest} from "../test";

/**
 * Gets a test by name.
 *
 * @param {string} name The test name
 * @returns {TwingTest} A MyTest instance or null if the test does not exist
 */
export const getTest = <Test extends TwingTest | TwingSynchronousTest>(
    tests: Map<string, Test>,
    name: string
): Test | null => {
    const result = tests.get(name);

    if (result) {
        return result;
    }

    for (let [pattern, test] of tests) {
        let count: number = 0;

        pattern = pattern.replace(/\*/g, function () {
            count++;

            return '(.*?)';
        });

        if (count) {
            const regExp = new RegExp('^' + pattern + '$', 'g');
            const match = regExp.exec(name);
            const matches = [];

            if (match) {
                for (let i = 1; i <= count; i++) {
                    matches.push(match[i]);
                }

                test.nativeArguments = matches;

                return test;
            }
        }
    }

    return null;
}
