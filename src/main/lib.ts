/* istanbul ignore file */

// cache
export type {TwingCache, TwingSynchronousCache} from "./lib/cache";

// error
export type {TwingError} from "./lib/error";
export type {TwingBaseError, TwingErrorLocation} from "./lib/error/base";
export type {TwingParsingError} from "./lib/error/parsing";
export type {TwingRuntimeError} from "./lib/error/runtime";

export {isATwingError} from "./lib/error";
export {createParsingError} from "./lib/error/parsing";
export {createRuntimeError} from "./lib/error/runtime";
export {createTemplateLoadingError} from "./lib/error/loader";

// loader
export type {
    TwingFilesystemLoader, TwingFilesystemLoaderFilesystem, TwingFilesystemLoaderFilesystemStats, TwingSynchronousFilesystemLoader, TwingSynchronousFilesystemLoaderFilesystem
} from "./lib/loader/filesystem";
export type {TwingArrayLoader, TwingSynchronousArrayLoader} from "./lib/loader/array";
export type {TwingChainLoader, TwingSynchronousChainLoader} from "./lib/loader/chain";
export type {TwingLoader, TwingSynchronousLoader} from "./lib/loader";

export {createFilesystemLoader, createSynchronousFilesystemLoader} from "./lib/loader/filesystem";
export {createArrayLoader, createSynchronousArrayLoader} from "./lib/loader/array";
export {createChainLoader, createSynchronousChainLoader} from "./lib/loader/chain";

// markup
export type {TwingMarkup} from "./lib/markup";

export {createMarkup, isAMarkup} from "./lib/markup";

// node
export type {
    TwingBaseNode,
    TwingBaseNodeAttributes,
    TwingBaseNodeChildren,
    TwingNode,
    TwingNodeAttributes,
    TwingNodeChildren,
    TwingNodeType,
    KeysOf
} from "./lib/node";
export type {TwingApplyNode, TwingApplyNodeAttributes, TwingApplyNodeChildren} from "./lib/node/apply";
export type {TwingAutoEscapeNode, TwingAutoEscapeNodeAttributes} from "./lib/node/auto-escape";
export type {TwingBlockNode, TwingBlockNodeAttributes} from "./lib/node/block";
export type {TwingBlockReferenceNode, TwingBlockReferenceNodeAttributes} from "./lib/node/block-reference";
export type {TwingCheckSecurityNode, TwingCheckSecurityNodeAttributes} from "./lib/node/check-security";
export type {TwingCheckToStringNode} from "./lib/node/check-to-string";
export type {TwingCommentNode, TwingCommentNodeAttributes} from "./lib/node/comment";
export type {TwingDeprecatedNode} from "./lib/node/deprecated";
export type {TwingDoNode} from "./lib/node/do";
export type {
    TwingExpressionNode, TwingBaseExpressionNode, TwingBaseExpressionNodeAttributes
} from "./lib/node/expression";
export type {TwingFlushNode} from "./lib/node/flush";
export type {TwingForNode, TwingForNodeAttributes, TwingForNodeChildren} from "./lib/node/for";
export type {TwingForLoopNode, TwingForLoopNodeAttributes} from "./lib/node/for-loop";
export type {TwingIfNode, TwingIfNodeChildren} from "./lib/node/if";
export type {TwingImportNode, TwingImportNodeAttributes} from "./lib/node/import";
export type {
    TwingBaseIncludeNode, TwingBaseIncludeNodeAttributes, TwingBaseIncludeNodeChildren
} from "./lib/node/include";
export type {TwingLineNode, TwingLineNodeAttributes} from "./lib/node/line";
export type {TwingMacroNode, TwingMacroNodeAttributes} from "./lib/node/macro";
export type {TwingPrintNode} from "./lib/node/print";
export type {TwingSandboxNode} from "./lib/node/sandbox";
export type {TwingSpacelessNode} from "./lib/node/spaceless";
export type {TwingTemplateNode, TwingTemplateNodeAttributes, TwingTemplateNodeChildren} from "./lib/node/template";
export type {TwingSetNode, TwingSetNodeAttributes} from "./lib/node/set";
export type {TwingTextNode, TwingBaseTextNode, TwingBaseTextNodeAttributes} from "./lib/node/text";
export type {TwingTraitNode} from "./lib/node/trait";
export type {TwingVerbatimNode} from "./lib/node/verbatim";
export type {TwingWithNode, TwingWithNodeAttributes, TwingWithNodeChildren} from "./lib/node/with";

export {createApplyNode} from "./lib/node/apply";
export {createAutoEscapeNode} from "./lib/node/auto-escape";
export {createBlockNode} from "./lib/node/block";
export {createBlockReferenceNode} from "./lib/node/block-reference";
export {createCheckSecurityNode} from "./lib/node/check-security";
export {createCheckToStringNode} from "./lib/node/check-to-string";
export {createCommentNode} from "./lib/node/comment";
export {createDeprecatedNode} from "./lib/node/deprecated";
export {createDoNode} from "./lib/node/do";
export {createFlushNode} from "./lib/node/flush";
export {createForNode} from "./lib/node/for";
export {createForLoopNode} from "./lib/node/for-loop";
export {createIfNode} from "./lib/node/if";
export {createImportNode} from "./lib/node/import";
export {createBaseIncludeNode} from "./lib/node/include";
export {createLineNode} from "./lib/node/line";
export {createMacroNode} from "./lib/node/macro";
export {createPrintNode} from "./lib/node/print";
export {createSandboxNode} from "./lib/node/sandbox";
export {createSetNode} from "./lib/node/set";
export {createSpacelessNode} from "./lib/node/spaceless";
export {createTemplateNode} from "./lib/node/template";
export {createTextNode} from "./lib/node/text";
export {createTraitNode} from "./lib/node/trait";
export {createVerbatimNode} from "./lib/node/verbatim";
export {createWithNode} from "./lib/node/with";

// node/expression
export type {TwingBaseArrayNode, TwingArrayNode} from "./lib/node/expression/array";
export type {TwingArrowFunctionNode} from "./lib/node/expression/arrow-function";
export type {TwingAssignmentNode, TwingAssignmentNodeAttributes} from "./lib/node/expression/assignment";
export type {
    TwingAttributeAccessorNode,
    TwingAttributeAccessorNodeAttributes,
    TwingAttributeAccessorNodeChildren,
    TwingAttributeAccessorCallType
} from "./lib/node/expression/attribute-accessor";
export type {TwingBaseBinaryNode, TwingBinaryNode} from "./lib/node/expression/binary";
export type {
    TwingBlockFunctionNode, TwingBlockFunctionNodeAttributes, TwingBlockFunctionNodeChildren
} from "./lib/node/expression/block-function";
export type {
    TwingBaseCallNode, TwingCallNode, TwingBaseCallNodeChildren, TwingBaseCallNodeAttributes
} from "./lib/node/expression/call";
export type {TwingBaseConditionalNode, TwingConditionalNode} from "./lib/node/expression/conditional";
export type {
    TwingConstantNode, TwingConstantNodeAttributes, TwingConstantNodeValue
} from "./lib/node/expression/constant";
export type {TwingEscapeNode, TwingEscapeNodeAttributes} from "./lib/node/expression/escape";
export type {TwingHashNode} from "./lib/node/expression/hash";
export type {TwingMethodCallNode, TwingMethodCallNodeAttributes} from "./lib/node/expression/method-call";
export type {TwingNameNode, TwingNameNodeAttributes} from "./lib/node/expression/name";
export type {TwingNullishCoalescingNode} from "./lib/node/expression/nullish-coalescing";
export type {TwingParentFunctionNode} from "./lib/node/expression/parent-function";
export type {TwingSpreadNode} from "./lib/node/expression/spread";
export type {TwingBaseUnaryNode, TwingUnaryNode} from "./lib/node/expression/unary";

export {createBaseArrayNode, createArrayNode} from "./lib/node/expression/array";
export {createArrowFunctionNode} from "./lib/node/expression/arrow-function";
export {createAssignmentNode} from "./lib/node/expression/assignment";
export {createAttributeAccessorNode} from "./lib/node/expression/attribute-accessor";
export {createBaseBinaryNode} from "./lib/node/expression/binary";
export {createBlockFunctionNode} from "./lib/node/expression/block-function";
export {createBaseCallNode} from "./lib/node/expression/call";
export {createBaseConditionalNode, createConditionalNode} from "./lib/node/expression/conditional";
export {createConstantNode} from "./lib/node/expression/constant";
export {createEscapeNode} from "./lib/node/expression/escape";
export {createHashNode} from "./lib/node/expression/hash";
export {createMethodCallNode} from "./lib/node/expression/method-call";
export {createNameNode} from "./lib/node/expression/name";
export {createNullishCoalescingNode} from "./lib/node/expression/nullish-coalescing";
export {createParentFunctionNode} from "./lib/node/expression/parent-function";
export {createSpreadNode} from "./lib/node/expression/spread";
export {createBaseUnaryNode} from "./lib/node/expression/unary";

// node/expression/binary
export type {TwingAddNode} from "./lib/node/expression/binary/add";
export type {TwingAndNode} from "./lib/node/expression/binary/and";
export type {TwingBitwiseAndNode} from "./lib/node/expression/binary/bitwise-and";
export type {TwingBitwiseOrNode} from "./lib/node/expression/binary/bitwise-or";
export type {TwingBitwiseXorNode} from "./lib/node/expression/binary/bitwise-xor";
export type {TwingConcatenateNode} from "./lib/node/expression/binary/concatenate";
export type {TwingDivideAndFloorNode} from "./lib/node/expression/binary/divide-and-floor";
export type {TwingDivideNode} from "./lib/node/expression/binary/divide";
export type {TwingEndsWithNode} from "./lib/node/expression/binary/ends-with";
export type {TwingHasEveryNode} from "./lib/node/expression/binary/has-every";
export type {TwingHasSomeNode} from "./lib/node/expression/binary/has-some";
export type {TwingIsEqualToNode} from "./lib/node/expression/binary/is-equal-to";
export type {TwingIsGreaterThanOrEqualToNode} from "./lib/node/expression/binary/is-greater-than-or-equal-to";
export type {TwingIsGreaterThanNode} from "./lib/node/expression/binary/is-greater-than";
export type {TwingIsInNode} from "./lib/node/expression/binary/is-in";
export type {TwingIsLessThanOrEqualToNode} from "./lib/node/expression/binary/is-less-than-or-equal-to";
export type {TwingIsLessThanNode} from "./lib/node/expression/binary/is-less-than";
export type {TwingIsNotEqualToNode} from "./lib/node/expression/binary/is-not-equal-to";
export type {TwingIsNotInNode} from "./lib/node/expression/binary/is-not-in";
export type {TwingMatchesNode} from "./lib/node/expression/binary/matches";
export type {TwingModuloNode} from "./lib/node/expression/binary/modulo";
export type {TwingMultiplyNode} from "./lib/node/expression/binary/multiply";
export type {TwingOrNode} from "./lib/node/expression/binary/or";
export type {TwingPowerNode} from "./lib/node/expression/binary/power";
export type {TwingRangeNode} from "./lib/node/expression/binary/range";
export type {TwingSpaceshipNode} from "./lib/node/expression/binary/spaceship";
export type {TwingStartsWithNode} from "./lib/node/expression/binary/starts-with";
export type {TwingSubtractNode} from "./lib/node/expression/binary/subtract";

export {createAddNode} from "./lib/node/expression/binary/add";
export {createAndNode} from "./lib/node/expression/binary/and";
export {createBitwiseAndNode} from "./lib/node/expression/binary/bitwise-and";
export {createBitwiseOrNode} from "./lib/node/expression/binary/bitwise-or";
export {createBitwiseXorNode} from "./lib/node/expression/binary/bitwise-xor";
export {createConcatenateNode} from "./lib/node/expression/binary/concatenate";
export {createDivideAndFloorNode} from "./lib/node/expression/binary/divide-and-floor";
export {createDivideNode} from "./lib/node/expression/binary/divide";
export {createEndsWithNode} from "./lib/node/expression/binary/ends-with";
export {createHasEveryNode} from "./lib/node/expression/binary/has-every";
export {createHasSomeNode} from "./lib/node/expression/binary/has-some";
export {createIsEqualNode} from "./lib/node/expression/binary/is-equal-to";
export {createIsGreaterThanNode} from "./lib/node/expression/binary/is-greater-than";
export {createIsGreaterThanOrEqualToNode} from "./lib/node/expression/binary/is-greater-than-or-equal-to";
export {createIsInNode} from "./lib/node/expression/binary/is-in";
export {createIsLessThanNode} from "./lib/node/expression/binary/is-less-than";
export {createIsLessThanOrEqualToNode} from "./lib/node/expression/binary/is-less-than-or-equal-to";
export {createIsNotEqualToNode} from "./lib/node/expression/binary/is-not-equal-to";
export {createIsNotInNode} from "./lib/node/expression/binary/is-not-in";
export {createMatchesNode} from "./lib/node/expression/binary/matches";
export {createModuloNode} from "./lib/node/expression/binary/modulo";
export {createMultiplyNode} from "./lib/node/expression/binary/multiply";
export {createOrNode} from "./lib/node/expression/binary/or";
export {createPowerNode} from "./lib/node/expression/binary/power";
export {createRangeNode} from "./lib/node/expression/binary/range";
export {createStartsWithNode} from "./lib/node/expression/binary/starts-with";
export {createSubtractNode} from "./lib/node/expression/binary/subtract";

// node/expression/call
export type {TwingFilterNode} from "./lib/node/expression/call/filter";
export type {TwingFunctionNode} from "./lib/node/expression/call/function";
export type {TwingTestNode} from "./lib/node/expression/call/test";

export {createFilterNode} from "./lib/node/expression/call/filter";
export {createFunctionNode} from "./lib/node/expression/call/function";
export {createTestNode} from "./lib/node/expression/call/test";

// node/expression/unary
export type {TwingNegativeNode} from "./lib/node/expression/unary/negative";
export type {TwingNotNode} from "./lib/node/expression/unary/not";
export type {TwingPositiveNode} from "./lib/node/expression/unary/positive";

export {createNegativeNode} from "./lib/node/expression/unary/negative";
export {createNotNode} from "./lib/node/expression/unary/not";
export {createPositiveNode} from "./lib/node/expression/unary/positive";

// node/include
export type {TwingEmbedNode, TwingEmbedNodeAttributes} from "./lib/node/include/embed";
export type {TwingIncludeNode, TwingIncludeNodeChildren} from "./lib/node/include/include";

export {createEmbedNode} from "./lib/node/include/embed";
export {createIncludeNode} from "./lib/node/include/include";

// node executors
export {executeNode, executeNodeSynchronously, type TwingNodeExecutor, type TwingSynchronousNodeExecutor} from "./lib/node-executor";

// tag handlers
export type {TwingTagHandler, TwingTokenParser} from "./lib/tag-handler";

export {createApplyTagHandler} from "./lib/tag-handler/apply";
export {createAutoEscapeTagHandler} from "./lib/tag-handler/auto-escape";
export {createBlockTagHandler} from "./lib/tag-handler/block";
export {createDeprecatedTagHandler} from "./lib/tag-handler/deprecated";
export {createDoTagHandler} from "./lib/tag-handler/do";
export {createEmbedTagHandler} from "./lib/tag-handler/embed";
export {createExtendsTagHandler} from "./lib/tag-handler/extends";
export {createFilterTagHandler} from "./lib/tag-handler/filter";
export {createFlushTagHandler} from "./lib/tag-handler/flush";
export {createForTagHandler} from "./lib/tag-handler/for";
export {createFromTagHandler} from "./lib/tag-handler/from";
export {createIfTagHandler} from "./lib/tag-handler/if";
export {createImportTagHandler} from "./lib/tag-handler/import";
export {createIncludeTagHandler} from "./lib/tag-handler/include";
export {createLineTagHandler} from "./lib/tag-handler/line";
export {createMacroTagHandler} from "./lib/tag-handler/macro";
export {createSandboxTagHandler} from "./lib/tag-handler/sandbox";
export {createSetTagHandler} from "./lib/tag-handler/set";
export {createSpacelessTagHandler} from "./lib/tag-handler/spaceless";
export {createUseTagHandler} from "./lib/tag-handler/use";
export {createVerbatimTagHandler} from "./lib/tag-handler/verbatim";
export {createWithTagHandler} from "./lib/tag-handler/with";

// core
export type {
    TwingCallable, TwingCallableArgument, TwingCallableWrapperOptions, TwingCallableWrapper, TwingSynchronousCallable, TwingSynchronousCallableWrapper
} from "./lib/callable-wrapper";
export {type TwingContext, createContext} from "./lib/context";
export type {TwingEnvironment, TwingEnvironmentOptions, TwingNumberFormat, TwingSynchronousEnvironment, TwingSynchronousEnvironmentOptions} from "./lib/environment";
export type {
    TwingEscapingStrategy, TwingEscapingStrategyHandler, TwingEscapingStrategyResolver
} from "./lib/escaping-strategy";
export type {TwingExecutionContext, TwingSynchronousExecutionContext} from "./lib/execution-context";
export type {TwingExtension, TwingSynchronousExtension} from "./lib/extension";
export type {TwingExtensionSet} from "./lib/extension-set";
export type {TwingFilter, TwingSynchronousFilter} from "./lib/filter";
export type {TwingFunction, TwingSynchronousFunction} from "./lib/function";
export type {TwingLexer} from "./lib/lexer";
export type {TwingNodeVisitor} from "./lib/node-visitor";
export type {
    TwingOperator, TwingOperatorAssociativity, TwingOperatorType, TwingOperatorExpressionFactory
} from "./lib/operator";
export {type TwingOutputBuffer, createOutputBuffer} from "./lib/output-buffer";
export type {TwingParser, TwingParserOptions} from "./lib/parser";
export type {TwingSandboxSecurityPolicy} from "./lib/sandbox/security-policy";
export type {TwingSource} from "./lib/source";
export type {TwingSourceMapRuntime} from "./lib/source-map-runtime";
export type {
    TwingTemplateAliases,
    TwingTemplateBlockMap,
    TwingTemplateBlockHandler,
    TwingTemplateMacroHandler,
    TwingSynchronousTemplateAliases,
    TwingSynchronousTemplateBlockHandler,
    TwingSynchronousTemplateBlockMap,
    TwingSynchronousTemplateMacroHandler
} from "./lib/template";
export type {TwingTest, TwingSynchronousTest} from "./lib/test";
export type {TwingTokenStream} from "./lib/token-stream";

export {createEnvironment, createSynchronousEnvironment} from "./lib/environment";
export {createExtensionSet} from "./lib/extension-set";
export {createFilter, createSynchronousFilter} from "./lib/filter";
export {createFunction, createSynchronousFunction} from "./lib/function";
export {createLexer} from "./lib/lexer";
export {createBaseNode, createNode, getChildren, getChildrenCount} from "./lib/node";
export {createOperator} from "./lib/operator";
export {createSandboxSecurityPolicy} from "./lib/sandbox/security-policy";
export {createSource} from "./lib/source";
export {createSourceMapRuntime} from "./lib/source-map-runtime";
export {type TwingTemplate, createTemplate, type TwingSynchronousTemplate, createSynchronousTemplate} from "./lib/template";
export {type TwingTemplateLoader, type TwingSynchronousTemplateLoader, createTemplateLoader, createSynchronousTemplateLoader} from "./lib/template-loader";
export {createTest, createSynchronousTest} from "./lib/test";
