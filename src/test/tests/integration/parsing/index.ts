import "./assigment-on-a-primitive-value";
import "./block-without-tag-name";
import "./invalid-named-argument";
import "./non-strict";
import "./strict";
import "./unexpected-token";
