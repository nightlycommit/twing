import {runTest} from "../../TestBase";
import {TwingCache} from "../../../../../main/lib/cache";
import {TwingTemplateNode} from "../../../../../main/lib/node/template";

const createCache = (): TwingCache => {
    const container = new Map<string, TwingTemplateNode>();

    return {
        getTimestamp: () => Promise.resolve(Number.POSITIVE_INFINITY),
        load: (key) => Promise.resolve(container.get(key) || null),
        write: (key, content) => Promise.resolve(container.set(key, content)).then()
    };
};

const cache = createCache();

const testCases: Array<[title: string, Record<string, any>, errorMessage: string]> = [
    ['record and method', {foo: {}}, 'TwingRuntimeError: Neither the property "bar" nor one of the methods bar()" or "getbar()"/"isbar()"/"hasbar()" exist and have public access in class "Object" in "index.twig" at line 1, column 4.'],
    ['map and method', {foo: new Map([['oof', 5]])}, 'TwingRuntimeError: Impossible to invoke a method ("bar") on an array in "index.twig" at line 1, column 4.'],
    ['array and method', {foo: [5]}, 'TwingRuntimeError: Impossible to invoke a method ("bar") on an array in "index.twig" at line 1, column 4.'],
    ['null and method', {foo: null}, 'TwingRuntimeError: Impossible to invoke a method ("bar") on a null variable in "index.twig" at line 1, column 4.'],
    ['number and method', {foo: 5}, 'TwingRuntimeError: Impossible to invoke a method ("bar") on a number variable ("5") in "index.twig" at line 1, column 4.'],
];

for (const [name, context, errorMessage] of testCases) {
    for (const strictVariables of [false, true]) {
        runTest({
            description: `method call honors the runtime strict check setting (${strictVariables}) with ${name}`,
            templates: {
                "index.twig": `{{ foo.bar() }}`
            },
            context,
            trimmedExpectation: strictVariables ? undefined : '',
            expectedErrorMessage: strictVariables ? errorMessage : undefined,
            strict: strictVariables,
            environmentOptions: {
                cache
            }
        });
    }
}
