import "./as-a-defined-test-operand";
import "./honors-runtime-sandbox-setting";
import "./honors-runtime-strict-variables-setting";
import "./class-instance-getter";
import "./on-context-when-missing";
import "./on-hash-when-missing";
