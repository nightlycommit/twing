import "./random";
import "./with-array-as-values";
import "./with-buffer-as-values";
import "./without-max";
import "./with-object-as-values";
import "./without-values";
