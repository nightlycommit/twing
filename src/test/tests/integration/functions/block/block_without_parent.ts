import TestBase, {runTest} from "../../TestBase";
import {createIntegrationTest} from "../../test";

class Test extends TestBase {
    getDescription() {
        return '"block" calling parent() with no definition in parent template';
    }
    
    getTemplates() {
        return {
            'index.twig': `
{% extends "parent.twig" %}
{% block label %}{{ parent() }}{% endblock %}`,
            'parent.twig': `
{{ block('label') }}`
        };
    }

    getExpectedErrorMessage() {
        return 'TwingRuntimeError: Block "label" should not call parent() in "index.twig" as the block does not exist in the parent template "parent.twig" in "index.twig" at line 3, column 21.';
    }
}

runTest(createIntegrationTest(new Test()));
