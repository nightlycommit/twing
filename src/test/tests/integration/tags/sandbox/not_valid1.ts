import TestBase, {runTest} from "../../TestBase";
import {createIntegrationTest} from "../../test";

class Test extends TestBase {
    getDescription() {
        return '"sandbox" tag';
    }

    getTemplates() {
        return {
            'foo.twig': `
foo`,
            'index.twig': `
{%- sandbox %}
    {%- include "foo.twig" %}
    a
{%- endsandbox %}`
        };
    }

    getExpectedErrorMessage() {
        return 'TwingParsingError: Only "include" tags are allowed within a "sandbox" section in "index.twig" at line 4, column 1.'
    }
}

runTest(createIntegrationTest(new Test));
