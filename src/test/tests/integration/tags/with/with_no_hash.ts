import TestBase, {runTest} from "../../TestBase";
import {createIntegrationTest} from "../../test";

class Test extends TestBase {
    getDescription() {
        return '"with" tag with an expression that is not a hash';
    }

    getTemplates() {
        return {
            'index.twig': `
{% with vars %}
    {{ foo }}{{ bar }}
{% endwith %}`
        };
    }

    getExpectedErrorMessage() {
        return 'TwingRuntimeError: Variables passed to the "with" tag must be a hash in "index.twig" at line 2, column 4.';
    }

    getContext() {
        return {
            vars: 'no-hash'
        }
    }
}

runTest(createIntegrationTest(new Test));
