import TestBase, {runTest} from "../../TestBase";
import {createIntegrationTest} from "../../test";

class Test extends TestBase {
    getDescription() {
        return '"import" tag with syntax error';
    }

    getTemplates() {
        return {
            'index.twig': `
{% import 'forms.twig' %}

{{ macros.parent() }}
`
        };
    }

    getExpectedErrorMessage() {
        return 'TwingParsingError: Unexpected token "end of statement block" of value "%}\n" ("name" expected with value "as") in "index.twig" at line 2, column 24.';
    }
}

runTest(createIntegrationTest(new Test));
