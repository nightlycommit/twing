import {runTest} from "../../TestBase";

runTest({
    description: '"deprecated" tag with a variable as message',
    templates: {
        'index.twig': '{% deprecated foo %}'
    },
    context: {
        foo: 'bar'
    },
    expectedDeprecationMessages: [
        'bar ("index.twig" at line 1, column 4)'
    ]
});
