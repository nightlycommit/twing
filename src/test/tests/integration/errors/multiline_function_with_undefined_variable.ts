import TestBase, {runTest} from "../TestBase";
import {createIntegrationTest} from "../test";

class Test extends TestBase {
    getDescription() {
        return 'Exception for multiline function with undefined variable';
    }

    getTemplates() {
        return {
            'index.twig': `
{{ include('foo',
   with_context=with_context
) }}`,
            'foo': `
Foo`
        };
    }

    getContext() {
        return {
            foobar: 'foobar'
        }
    }

    getExpectedErrorMessage() {
        return 'TwingRuntimeError: Variable "with_context" does not exist in "index.twig" at line 3, column 17.';
    }
}

runTest(createIntegrationTest(new Test));
