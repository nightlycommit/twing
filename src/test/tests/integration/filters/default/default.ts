import TestBase, {runTest} from "../../TestBase";
import {createIntegrationTest} from "../../test";

class Foo {
    public array: any[];
    public position: number;

    constructor() {
        this.array = [];
        this.position = 0;
    }

    * [Symbol.iterator]() {
        yield 1;
        yield 2;
    }

    bar(param1: string | null = null, param2: string | null = null) {
        return 'bar' + (param1 ? '_' + param1 : '') + (param2 ? '-' + param2 : '');
    }

    getFoo() {
        return 'foo';
    }

    getSelf() {
        return this;
    }

    is() {
        return 'is';
    }

    in() {
        return 'in';
    }

    not() {
        return 'not';
    }
}

class Test extends TestBase {
    getDescription() {
        return '"default" filter';
    }

    getTemplates() {
        return {
            'index.twig': `
Variable:
{{ definedVar                  |default('default') is same as('default') ? 'ko' : 'ok' }}
{{ zeroVar                     |default('default') is same as('default') ? 'ko' : 'ok' }}
{{ emptyVar                    |default('default') is same as('default') ? 'ok' : 'ko' }}
{{ nullVar                     |default('default') is same as('default') ? 'ok' : 'ko' }}
{{ undefinedVar                |default('default') is same as('default') ? 'ok' : 'ko' }}
Array access:
{{ nested.definedVar           |default('default') is same as('default') ? 'ko' : 'ok' }}
{{ nested['definedVar']        |default('default') is same as('default') ? 'ko' : 'ok' }}
{{ nested.zeroVar              |default('default') is same as('default') ? 'ko' : 'ok' }}
{{ nested.emptyVar             |default('default') is same as('default') ? 'ok' : 'ko' }}
{{ nested.nullVar              |default('default') is same as('default') ? 'ok' : 'ko' }}
{{ nested.undefinedVar         |default('default') is same as('default') ? 'ok' : 'ko' }}
{{ nested['undefinedVar']      |default('default') is same as('default') ? 'ok' : 'ko' }}
{{ undefined['undefined']      |default('default') is same as('default') ? 'ok' : 'ko' }}
{{ undefinedVar.foo            |default('default') is same as('default') ? 'ok' : 'ko' }}
Plain values:
{{ 'defined'                   |default('default') is same as('default') ? 'ko' : 'ok' }}
{{ 0                           |default('default') is same as('default') ? 'ko' : 'ok' }}
{{ ''                          |default('default') is same as('default') ? 'ok' : 'ko' }}
{{ null                        |default('default') is same as('default') ? 'ok' : 'ko' }}
Precedence:
{{ 'o' ~ nullVar               |default('k') }}
{{ 'o' ~ nested.nullVar        |default('k') }}
Object methods:
{{ object.foo                  |default('default') is same as('default') ? 'ko' : 'ok' }}
{{ object.undefinedMethod      |default('default') is same as('default') ? 'ok' : 'ko' }}
{{ object.getFoo()             |default('default') is same as('default') ? 'ko' : 'ok' }}
{{ object.getFoo('a')          |default('default') is same as('default') ? 'ko' : 'ok' }}
{{ object.undefinedMethod()    |default('default') is same as('default') ? 'ok' : 'ko' }}
{{ object.undefinedMethod('a') |default('default') is same as('default') ? 'ok' : 'ko' }}
Deep nested:
{{ nested.undefinedVar.foo.bar |default('default') is same as('default') ? 'ok' : 'ko' }}
{{ nested.definedArray.0       |default('default') is same as('default') ? 'ko' : 'ok' }}
{{ nested['definedArray'][0]   |default('default') is same as('default') ? 'ko' : 'ok' }}
{{ nested['undefinedVar'][0]   |default('default') is same as('default') ? 'ok' : 'ko' }}
{{ undefined['undefined'][0]   |default('default') is same as('default') ? 'ok' : 'ko' }}
{{ object.self.foo             |default('default') is same as('default') ? 'ko' : 'ok' }}
{{ object.self.undefinedMethod |default('default') is same as('default') ? 'ok' : 'ko' }}
{{ object.undefinedMethod.self |default('default') is same as('default') ? 'ok' : 'ko' }}
`};
    }

    getExpected() {
        return `
Variable:
ok
ok
ok
ok
ok
Array access:
ok
ok
ok
ok
ok
ok
ok
ok
ok
Plain values:
ok
ok
ok
ok
Precedence:
ok
ok
Object methods:
ok
ok
ok
ok
ok
ok
Deep nested:
ok
ok
ok
ok
ok
ok
ok
ok
`;
    }

    getContext() {
        return {
            definedVar: 'defined',
            zeroVar: 0,
            emptyVar: '',
            nullVar: null,
            nested: {
                definedVar: 'defined',
                zeroVar: 0,
                emptyVar: '',
                nullVar: null,
                definedArray: [0],
            },
            object: new Foo(),
        } as any;
    }

    getStrict(): boolean {
        return false;
    }
}

runTest(createIntegrationTest(new Test()));
